# Manuscripts TSLint config

Shared TSLint config for Manuscripts projects.

## Usage

Use the following in a project's `tslint.json` file:

```json
{
  "extends": "@manuscripts/tslint-config"
}
```

## License header

The `file-header` rule enforces a short Apache 2.0 license header at the start of every file. 

To add the missing file header to all files in a project, run `tslint --project . --fix`

To disable this rule, add the following to a project's `tslint.json`:

```
  "rules": {
    "file-header": false
  }
```
