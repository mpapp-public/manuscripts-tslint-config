const { licenseHeader, licenseMatcher } = require('./file-header')

module.exports = {
  extends: [
    'tslint:latest',
    'tslint-react',
    'tslint-config-standard',
    'tslint-config-prettier',
  ],
  rulesDirectory: ['tslint-plugin-prettier'],
  rules: {
    'cyclomatic-complexity': [true, 8],
    'file-header': [true, licenseMatcher, licenseHeader ],
    'interface-name': [true, 'never-prefix'],
    'jsx-no-lambda': false,
    'max-classes-per-file': false,
    'no-any': true,
    'no-implicit-dependencies': [true, 'dev'],
    'no-shadowed-variable': false,
    'no-submodule-imports': false,
    'no-unused-variable': false,
    'object-literal-sort-keys': false,
    'only-arrow-functions': true,
    prettier: true,
    typedef: true,
  },
}
